﻿using FFXI_Parser_Desktop.Models;
using LiteDB;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFXI_Parser_Desktop
{
    static class DatabaseActions
    {
        static private LiteDatabase db;
        static private LiteCollection<ParseModel> parsedActions;
        static private LiteCollection<ExpModel> parsedExp;

        static public void OpenDatabase()
        {
            //https://www.litedb.org/

            db = new LiteDatabase(@"MyData.db");
            parsedActions = db.GetCollection<ParseModel>("ParsedCollection");
            parsedExp = db.GetCollection<ExpModel>("ExpCollection");
        }

        static public void SaveParsedActionToDatabase(ParseModel model)
        {
            //Insert new document (Id will be auto-incremented)
            parsedActions.Insert(model);
        }

        static public void SaveParsedExpToDatabase(ExpModel model)
        {
            //Insert new document (Id will be auto-incremented)
            parsedExp.Insert(model);
        }

        static public void DeleteAllFromDatabase()
        {
            parsedActions.Delete(x => x.Time < DateTime.MaxValue);
            db.DropCollection("ParsedCollection");
            db.DropCollection("ExpCollection");
        }

        static public BlockingCollection<ParseModel> GetActions()
        {
            List<ParseModel> results = parsedActions.FindAll().ToList();
            BlockingCollection<ParseModel> ParsedObjects = new BlockingCollection<ParseModel>(new ConcurrentQueue<ParseModel>(results));

            return ParsedObjects;
        }

        static public BlockingCollection<ExpModel> GetExp()
        {
            List<ExpModel> expResult = parsedExp.FindAll().ToList();
            BlockingCollection<ExpModel> ExpList = new BlockingCollection<ExpModel>(new ConcurrentQueue<ExpModel>(expResult));

            return ExpList;
        }
    }
}
