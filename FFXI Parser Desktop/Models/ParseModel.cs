﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFXI_Parser_Desktop.Models
{
    public class ParseModel
    {
        public string Attacker { get; set; }
        public string Target { get; set; }
        public int Damage { get; set; }
        public string OrginText { get; set; }
        public AttackType AtkType { get; set; }
        public AttackResult AtkRes { get; set; }
        public DateTime Time { get; set; }
        public int _id { get; set; }
    }

    class AggregatedData
    {
        public string PlayerName { get; set; }
        public int TotalDamage { get; set; }
        public string MeleeAccuracy { get; set; }
        public string MeleeHits { get; set; }
        public string TotalRangedAccuracy { get; set; }
        public string TotalEvasion { get; set; }
        public string EvasionHits { get; set; }
        public double DPS { get; set; }
    }

    public class ExpModel
    {
        public DateTime Time { get; set; }
        public int Exp { get; set; }
        public int _id { get; set; }
    }

    class ExcludedPlayerModel
    {
        public string PlayerName { get; set; }
    }

    public enum AttackType
    {
        Melee,
        Range,
        Magic,
        WeaponSkill,
        Critical,
        Other
    }

    public enum AttackResult
    {
        Hit,
        Miss,
        Heal,
        Other
    }
}
