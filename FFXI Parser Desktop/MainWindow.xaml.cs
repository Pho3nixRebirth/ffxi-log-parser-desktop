﻿using FFXI_Parser_Desktop.Models;
using LiteDB;
using Microsoft.Win32;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace FFXI_Parser_Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FileSystemWatcher fs;
        BlockingCollection<ParseModel> ParsedObjects = new BlockingCollection<ParseModel>();
        BlockingCollection<ExpModel> ExpList = new BlockingCollection<ExpModel>();
        ObservableCollection<AggregatedData> AggregatedDataList = new ObservableCollection<AggregatedData>();
        ObservableCollection<ExcludedPlayerModel> ExcludedPlayers = new ObservableCollection<ExcludedPlayerModel>();
        DateTime fromTimeCached = DateTime.MinValue;
        DateTime toTimeCached = DateTime.MaxValue;
        public static List<string> ErrorLog = new List<string>();
        string path;
        bool CommitNew = true;

        public MainWindow()
        {
            InitializeComponent();

            LogsPath.Text = @"V:\NasomiXI\SquareEnix\FINAL FANTASY XI\TEMP\Diidii";
            if (LogsPath.Text != "")
            {
                RunButton.IsEnabled = true;
                path = LogsPath.Text;
            }

            ParseResultDataGrid.ItemsSource = AggregatedDataList;
            ExcludedPlayersDataGrid.ItemsSource = ExcludedPlayers;
            FromDate.Text = DateTime.Now.Date.ToString();
            ToDate.Text = DateTime.Now.Date.ToString();
            FromTime.Value = DateTime.Today;
            ToTime.Value = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);

            DatabaseActions.OpenDatabase();
            ParsedObjects = DatabaseActions.GetActions();
            ExpList = DatabaseActions.GetExp();
        }

        private void CreateFileSystemWatcher()
        {
            //the folder to be watched
            string watchingFolder = LogsPath.Text;

            if (fs == null)
            {
                //initialize the filesystem watcher
                fs = new FileSystemWatcher(watchingFolder, "*.*")
                {
                    EnableRaisingEvents = false,
                    IncludeSubdirectories = true
                };

                //event will check for files in watching folder
                fs.Created += new FileSystemEventHandler(ReadLogs);
                fs.Changed += new FileSystemEventHandler(ReadLogs);
            }
            else
            {
                fs.EnableRaisingEvents = true;
            }
        }

        private void ReadLogs(object fileChanged, FileSystemEventArgs changeEvent)
        {
            if (fileChanged != null)
            {
                string fileName = changeEvent.FullPath;

                Parse(fileName);

                RefreshUI();
            }
        }

        #region Default Controller events
        private void GetLogsDir_Click(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    var file = fbd.SelectedPath;
                    LogsPath.Text = file;

                    RunButton.IsEnabled = true;
                }
            }
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            ReadLogs(null, null);

            if (RunButton.Content.ToString() != "Stop")
            {
                CreateFileSystemWatcher();

                fs.EnableRaisingEvents = true;
                RunningProgress.IsIndeterminate = true;
                RunButton.Content = "Stop";
            }
            else
            {
                fs.EnableRaisingEvents = false;
                RunningProgress.IsIndeterminate = false;
                RunButton.Content = "Run";
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshUI();
        }

        private void ParseAllButton_Click(object sender, RoutedEventArgs e)
        {
            string filepath = LogsPath.Text;
            DirectoryInfo d = new DirectoryInfo(filepath);

            foreach (var file in d.GetFiles("*.log"))
            {
                Console.WriteLine(file.FullName);

                Parse(file.FullName);
                RefreshUI();
            }
        }

        private void PurgeDBButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("Do you want to purge the database and delete all information?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.Yes)
            {
                DatabaseActions.DeleteAllFromDatabase();
                ParsedObjects.Dispose();
                ExpList.Dispose();

                RefreshUI();
            }
        }

        private void SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePicker tempPicker = (DatePicker)sender;
            string name = tempPicker.Name;

            if (tempPicker.SelectedDate == null)
            {
                tempPicker.SelectedDate = DateTime.Today;
            }

            if (name.Contains("From"))
            {
                DateTime tmpDT = new DateTime(tempPicker.SelectedDate.Value.Year, tempPicker.SelectedDate.Value.Month, tempPicker.SelectedDate.Value.Day, fromTimeCached.Hour, fromTimeCached.Minute, fromTimeCached.Second);
                fromTimeCached = tmpDT;
            }
            else
            {
                DateTime tmpDT = new DateTime(tempPicker.SelectedDate.Value.Year, tempPicker.SelectedDate.Value.Month, tempPicker.SelectedDate.Value.Day, toTimeCached.Hour, toTimeCached.Minute, toTimeCached.Second);
                toTimeCached = tmpDT;
            }
        }

        private void SelectedTimeChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TimePicker tempPicker = (TimePicker)sender;
            string name = tempPicker.Name;

            if (tempPicker.Value == null)
            {
                if (name.Contains("From"))
                {
                    tempPicker.Value = DateTime.Today;
                }
                else
                {
                    tempPicker.Value = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }

            if (name.Contains("From"))
            {
                DateTime tmpDT = new DateTime(fromTimeCached.Year, fromTimeCached.Month, fromTimeCached.Day, tempPicker.Value.Value.Hour, tempPicker.Value.Value.Minute, tempPicker.Value.Value.Second);
                fromTimeCached = tmpDT;
            }
            else
            {
                DateTime tmpDT = new DateTime(fromTimeCached.Year, fromTimeCached.Month, fromTimeCached.Day, tempPicker.Value.Value.Hour, tempPicker.Value.Value.Minute, tempPicker.Value.Value.Second);
                toTimeCached = tmpDT;
            }
        }

        private void ExcludeMenyItem_Click(object sender, RoutedEventArgs e)
        {
            //Get the clicked MenuItem
            var menuItem = (System.Windows.Controls.MenuItem)sender;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (System.Windows.Controls.ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (System.Windows.Controls.DataGrid)contextMenu.PlacementTarget;

            //Get the underlying item, that you cast to your object that is bound
            //to the DataGrid (and has subject and state as property)
            var toExcludeFromList = (AggregatedData)item.SelectedCells[0].Item;

            ExcludedPlayerModel newExlPl = new ExcludedPlayerModel
            {
                PlayerName = toExcludeFromList.PlayerName
            };

            if (ExcludedPlayers.FirstOrDefault(x => x.PlayerName == newExlPl.PlayerName) == null)
            {
                ExcludedPlayers.Add(newExlPl);
            }

            ExcludedPlayersDataGrid.Items.Refresh();
            ParseResultDataGrid.Items.Refresh();
        }

        private void AddPlayerMenyItem_Click(object sender, RoutedEventArgs e)
        {
            //Get the clicked MenuItem
            var menuItem = (System.Windows.Controls.MenuItem)sender;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (System.Windows.Controls.ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (System.Windows.Controls.DataGrid)contextMenu.PlacementTarget;

            if (item.Items.Count > 0)
            {
                //Get the underlying item, that you cast to your object that is bound
                //to the DataGrid (and has subject and state as property)
                var toExcludeFromList = (ExcludedPlayerModel)item.SelectedCells[0].Item;

                ExcludedPlayers.Remove(toExcludeFromList);

                ExcludedPlayersDataGrid.Items.Refresh();
                ParseResultDataGrid.Items.Refresh();
            }
        }

        private void HistoryMenuItem_Click(object sender, RoutedEventArgs e)
        {
            List<ParseModel> tempCol = new List<ParseModel>(ParsedObjects.ToList());

            History histWindow = new History(tempCol, fromTimeCached, toTimeCached);

            histWindow.Show();
        }

        private void ParseResultDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            Style s = new Style();
            s.Setters.Add(new Setter(TextBlock.TextAlignmentProperty, TextAlignment.Right));

            switch (e.PropertyName)
            {
                case "PlayerName":
                    e.Column.Header = "Actor";
                    break;
                case "TotalDamage":
                    e.Column.Header = "Damage";
                    break;
                case "TotalMeleeAccuracy":
                    e.Column.Header = "Melee Accuracy";
                    e.Column.CellStyle = s;
                    break;
                case "TotalRangedAccuracy":
                    e.Column.Header = "Ranged Accuracy";
                    break;
                case "TotalEvasion":
                    e.Column.Header = "Evasion";
                    break;
            }
        }

        private void DetailsMenyItem_Click(object sender, RoutedEventArgs e)
        {
            //Get the clicked MenuItem
            var menuItem = (System.Windows.Controls.MenuItem)sender;

            //Get the ContextMenu to which the menuItem belongs
            var contextMenu = (System.Windows.Controls.ContextMenu)menuItem.Parent;

            //Find the placementTarget
            var item = (System.Windows.Controls.DataGrid)contextMenu.PlacementTarget;

            //Get the underlying item, that you cast to your object that is bound
            //to the DataGrid (and has subject and state as property)
            var actorToGetDetailsOn = (AggregatedData)item.SelectedCells[0].Item;

            List<ParseModel> tempCol = new List<ParseModel>(ParsedObjects.Where(x => x.Attacker == actorToGetDetailsOn.PlayerName));

            History histWindow = new History(tempCol, fromTimeCached, toTimeCached);

            histWindow.Show();
        }

        private void LabelExpTitle_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            List<ExpModel> tempCol = new List<ExpModel>(ExpList.ToList());

            History histWindow = new History(null, fromTimeCached, toTimeCached, tempCol);

            histWindow.Show();
        }
        #endregion

        #region Custom Methods
        private void Parse(string fileChanged)
        {
            List<Tuple<DateTime, string>> actions = GetActionsFromLogs(fileChanged);

            foreach (Tuple<DateTime, string> actionItem in actions)
            {
                try
                {
                    if (actionItem.Item2.Contains("experience"))
                    {
                        int firstIndex = actionItem.Item2.IndexOf("gains") + 6;
                        int secondIndex = actionItem.Item2.IndexOf(" experience");

                        string exp = actionItem.Item2.Substring(firstIndex, secondIndex - firstIndex);

                        if (ExpList.FirstOrDefault(x => x.Time == actionItem.Item1) == null)
                        {
                            if (int.TryParse(exp, out int expInt))
                            {
                                if (ExpList.FirstOrDefault(x => x.Time == actionItem.Item1) == null)
                                {
                                    ExpModel tempModel = new ExpModel()
                                    {
                                        Time = actionItem.Item1,
                                        Exp = expInt
                                    };

                                    ExpList.Add(tempModel);
                                    DatabaseActions.SaveParsedExpToDatabase(tempModel);
                                }
                            }
                        }
                    }
                    else
                    {
                        ParseModel obj = CreateNewParseItem(actionItem.Item2);

                        if (obj != null)
                        {
                            obj.Time = actionItem.Item1;

                            CheckForDuplicatesAndAdd(obj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.Add(actionItem.Item2 + ", message: " + ex.Message);
                    continue;
                }
            }
        }

        private void RefreshUI()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                CreateNewAggregatedData();
                RefreshStatsScreen();

                StatusBarLabel.Content = "Lastest update: " + DateTime.Now.ToString("HH:mm:ss");
            }));

            //Dispatcher.Invoke(() =>
            //    {
            //        CreateNewAggregatedData();
            //        RefreshStatsScreen();

            //        StatusBarLabel.Content = "Lastest update in directory found: " + DateTime.Now.ToString("HH:mm:ss");
            //    });

        }

        private List<Tuple<DateTime, string>> GetActionsFromLogs(string fileChanged)
        {
            List<Tuple<DateTime, string>> returnList = new List<Tuple<DateTime, string>>();
            List<string> logFiles = new List<string>();
            List<string> CompleteLogList = new List<string>();
            DirectoryInfo d = new DirectoryInfo(path);

            //foreach (FileInfo file in d.GetFiles("*.log"))
            //{
            //    logFiles.Add(System.IO.Path.Combine(d.FullName, file.FullName));
            //}

            if (File.Exists(fileChanged))
            {
                logFiles.Add(fileChanged);
            }

            string completeString = "";
            foreach (string logItem in logFiles)
            {
                try
                {
                    // Create the streams.
                    MemoryStream destination = new MemoryStream();


                    FileStream fileStream = new FileStream(logItem, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                    using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        completeString += streamReader.ReadToEnd();
                    }

                    fileStream.Close();
                }
                catch (Exception)
                {
                    continue;
                }
            }

            char[] delimiterChars = new char[] { '[', '.', ',' };
            completeString = new string(completeString.Where(c => !char.IsControl(c)).ToArray());
            completeString = new string(completeString.Where(c => !char.IsSymbol(c)).ToArray());
            string[] cutString = completeString.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries).Where(w => w.Any(c => Char.IsDigit(c))).Where(w => w.Any(c => !Char.IsWhiteSpace(c))).ToArray();
            List<string> filteredLines = cutString.Where(l => l.Contains(" ")).ToList();

            for (int i = 1; i < filteredLines.Count; i++)
            {
                if (!filteredLines[i].Contains("]"))
                {
                    if (CompleteLogList.Count > 0)
                    {
                        CompleteLogList[CompleteLogList.Count - 1] = CompleteLogList[CompleteLogList.Count - 1] + " " + filteredLines[i];
                    }
                }
                else if (filteredLines[i].Contains("Additional"))
                {
                    string[] additionalEffectString = filteredLines[i].Split(']');

                    //Find the right(iish) attacker
                    for (int j = CompleteLogList.Count - 1; j > 0; j--)
                    {
                        //Can additional effects only occur on normal hits?
                        if (CompleteLogList[j].Contains("hits"))
                        {
                            CompleteLogList[j] = CompleteLogList[j] + " #" + additionalEffectString[1].Substring(1);
                            break;
                        }
                    }
                }
                else
                {
                    CompleteLogList.Add("[" + filteredLines[i]);
                }
            }

            string[] keyWords = new string[] { "damage", "misses", "absorbs", "recovers", "experience" };
            foreach (string item in CompleteLogList)
            {
                foreach (string key in keyWords)
                {
                    if (item.Contains(key))
                    {
                        try
                        {
                            delimiterChars = new char[] { '[', ']' };
                            string[] cutLogInput = item.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                            DateTime outP = DateTime.MinValue;

                            if (DateTime.TryParse(cutLogInput[0], out outP))
                            {
                                returnList.Add(new Tuple<DateTime, string>(outP, cutLogInput[1]));
                            }
                        }
                        catch (Exception)
                        {
                            continue;
                        }

                        break;
                    }
                }
            }

            return returnList;
        }

        private ParseModel CreateNewParseItem(string action)
        {
            string[] actors = GetActorsFromLine(action);

            if (actors[0] != "")
            {
                ParseModel obj = new ParseModel()
                {
                    AtkRes = GetResultFromLine(action),
                    AtkType = GetTypeFromLine(action),
                    OrginText = action,
                    Attacker = actors[0],
                    Target = actors[1],
                    Damage = GetDamageFromLine(action)
                };

                return obj;
            }

            return null;
        }

        private string[] GetActorsFromLine(string line)
        {
            string[] actors = new string[2];
            string actor = "";
            string target = "";

            if (line.Contains("hits") && !line.Contains("ranged attack"))
            {
                int firstIndex = line.IndexOf("hits");
                int secondIndex = line.IndexOf(" for");
                actor = line.Substring(0, firstIndex);
                target = line.Substring(firstIndex + 5, secondIndex - (firstIndex + 5));
            }

            if (line.Contains("ranged attack"))
            {
                int rangedIndex = line.IndexOf("ranged attack");
                int secondIndex = line.IndexOf("'s");
                int targetIndexOne = line.IndexOf("the ") + 4;
                int targetIndexTwo = line.IndexOf(" for ");

                if (line.Contains("scores a critical"))
                {
                    targetIndexOne = line.IndexOf("The ") + 4;
                    targetIndexTwo = line.IndexOf(" takes");
                }

                actor = line.Substring(0, rangedIndex);

                if (secondIndex > 0)
                {
                    actor = line.Substring(0, secondIndex);
                }

                if (targetIndexTwo > 0)
                {
                    target = line.Substring(targetIndexOne, targetIndexTwo - targetIndexOne);
                }
            }

            if (line.Contains("scores a critical") && !line.Contains("ranged attack"))
            {
                int index = line.IndexOf("scores a critical hit");
                int hitIndex = line.IndexOf("hit");
                int secondIndex = line.IndexOf(" ", hitIndex);
                int thirdIndex = line.IndexOf(" take");
                actor = line.Substring(0, index);
                target = line.Substring(secondIndex, thirdIndex - secondIndex);
            }

            if (line.Contains("misses") && !line.Contains("ranged attack"))
            {
                int index = line.IndexOf("misses");
                actor = line.Substring(0, index);
                target = line.Substring(index + 7, line.Length - (index + 7));

                char[] delimiterChars = new char[] { ')', '(', '"', '%', '-' };
                if (target.IndexOfAny(delimiterChars) != -1)
                {
                    int cutIndex = int.MaxValue;
                    for (int i = 0; i < target.Length; i++)
                    {
                        if (delimiterChars.Contains(target[i]))
                        {
                            cutIndex = i;
                            string findLastSpace = target.Substring(0, i);
                            int lastIndex = findLastSpace.LastIndexOf(' ');
                            target = findLastSpace.Substring(0, lastIndex);
                        }
                    }
                }
            }

            //TODO
            if (line.Contains("casts"))
            {
                int index = line.IndexOf("casts");
                actor = line.Substring(0, index);

                if (!line.Contains("recover"))
                {

                }
            }

            if (line.Contains("'s shadows"))
            {
                int index = line.IndexOf(" shadows");
                int indexBefore = line.IndexOf(" of ") + 4;
                target = line.Substring(indexBefore, index - indexBefore);

                int removeAt = target.IndexOf("'s");
                if (removeAt > 0)
                {
                    actor = "Mob";
                    target = target.Remove(removeAt);
                }
            }

            //TODO
            if (line.Contains("uses"))
            {
                int rangedIndex = line.IndexOf("uses");
                actor = line.Substring(0, rangedIndex);
            }

            // Won't work with AOE Magic from players??
            if (line.Contains(" points of damage") || line.Contains(" point of damage"))
            {
                if (!line.Contains("hits") && !line.Contains("scores") && !line.Contains("casts") && !line.Contains("uses"))
                {
                    actor = "Mob";
                    target = line.Substring(1, line.IndexOf(" ", 1) - 1);
                }
            }

            if (actor.Length > 0)
            {
                if (actor.Substring(actor.Length - 1, 1) == " ")
                {
                    actor = actor.Remove(actor.Length - 1, 1);
                }

                if (actor.Substring(0, 1) == " ")
                {
                    actor = actor.Remove(0, 1);
                }

                actors[0] = actor;
            }

            if (target.Length > 0)
            {
                if (target.Substring(0, 3) == "The")
                {
                    target = target.Remove(0, 3);
                }
                else if (target.Length > 5 && target.Substring(0, 4) == "the ")
                {
                    target = target.Remove(0, 4);
                }
                else if (target.Contains(" The "))
                {
                    if (target.Substring(0, 5) == " The ")
                    {
                        target = target.Remove(0, 5);
                    }
                }

                actors[1] = target;
            }

            return actors;
        }

        private AttackResult GetResultFromLine(string line)
        {
            AttackResult ar;

            if (line.Contains("damage"))
            {
                ar = AttackResult.Hit;
                return ar;
            }
            else if (line.Contains("misses"))
            {
                ar = AttackResult.Miss;
                return ar;
            }
            else if (line.Contains("parries") | line.Contains("shadows"))
            {
                ar = AttackResult.Hit;
                return ar;
            }
            else if (line.Contains("recover"))
            {
                ar = AttackResult.Heal;
                return ar;
            }
            else
            {
                ar = AttackResult.Other;
                return ar;
            }
        }

        private AttackType GetTypeFromLine(string line)
        {
            AttackType at;

            if (line.Contains("ranged attack"))
            {
                at = AttackType.Range;
                return at;
            }
            else if (line.Contains("hit"))
            {
                at = AttackType.Melee;
                return at;
            }
            else if (line.Contains("parries") | line.Contains("shadows"))
            {
                at = AttackType.Melee;
                return at;
            }
            else if (line.Contains("critical hit!"))
            {
                at = AttackType.Critical;
                return at;
            }
            else if (line.Contains("casts"))
            {
                at = AttackType.Magic;
                return at;
            }
            else if (line.Contains("misses"))
            {
                at = AttackType.Melee;
                return at;
            }
            else
            {
                at = AttackType.Other;
                return at;
            }
        }

        private int GetDamageFromLine(string line)
        {
            int damage = 0;

            try
            {
                if (line.Contains("damage"))
                {
                    if (line.Contains("points"))
                    {
                        int damageIndex = line.IndexOf("points") - 1;
                        int numberIndex = line.Substring(0, damageIndex).LastIndexOf(" ") + 1;
                        string damageString = line.Substring(numberIndex, damageIndex - numberIndex);

                        damage = int.Parse(damageString);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }



            return damage;
        }

        private void CheckForDuplicatesAndAdd(ParseModel obj)
        {
            List<ParseModel> tempList = new List<ParseModel>(ParsedObjects);

            //Check for duplicate with the timestamp and original action text
            var dupl = tempList.Where(i => i.Time == obj.Time).Where(i => i.OrginText == obj.OrginText).FirstOrDefault();

            if (dupl == null)
            {
                ParsedObjects.Add(obj);

                if (CommitNew)
                {
                    DatabaseActions.SaveParsedActionToDatabase(obj);
                }
            }
        }

        private void CreateNewAggregatedData()
        {
            List<ParseModel> tempList = new List<ParseModel>(ParsedObjects.Where(x => x.Time > fromTimeCached && x.Time < toTimeCached).ToList());
            AggregatedDataList.Clear();


            foreach (string item in tempList.Select(x => x.Attacker).Distinct())
            {
                if (!ExcludedPlayers.Any(x => x.PlayerName == item))
                {
                    if (!AggregatedDataList.Any(x => x.PlayerName == item))
                    {
                        AggregatedData tempObject = new AggregatedData()
                        {
                            PlayerName = item
                        };

                        string[] AccHits = CalculateMeeleAccuracy(item);
                        string[] EvaHits = CalculateEvasion(item);

                        tempObject.TotalDamage = tempList.Where(x => x.Attacker == item).Select(x => x.Damage).Sum();
                        tempObject.MeleeAccuracy = AccHits[0];
                        tempObject.MeleeHits = AccHits[1];
                        tempObject.TotalRangedAccuracy = CalculateRangedAccuracy(item);
                        tempObject.TotalEvasion = EvaHits[0];
                        tempObject.EvasionHits = EvaHits[1];
                        tempObject.DPS = CalculateDPS(item);

                        AggregatedDataList.Add(tempObject);
                    }
                }
            }



            //foreach (ParseModel item in tempList)
            //{
            //    if (!ExcludedPlayers.Any(x => x.PlayerName == item.Attacker))
            //    {
            //        if (item.Time > fromTimeCached && item.Time < toTimeCached)
            //        {
            //            if (!AggregatedDataList.Any(x => x.PlayerName == item.Attacker))
            //            {
            //                AggregatedData tempObj = new AggregatedData()
            //                {
            //                    PlayerName = item.Attacker
            //                };

            //                AggregatedDataList.Add(tempObj);
            //            }


            //            AggregatedData dataObj = AggregatedDataList.First(x => x.PlayerName == item.Attacker);
            //            string[] AccHits = CalculateMeeleAccuracy(dataObj.PlayerName);
            //            string[] EvaHits = CalculateEvasion(dataObj.PlayerName);

            //            dataObj.TotalDamage += item.Damage;
            //            dataObj.MeleeAccuracy = AccHits[0];
            //            dataObj.MeleeHits = AccHits[1];
            //            dataObj.TotalRangedAccuracy =
            //            dataObj.TotalEvasion = EvaHits[0];
            //            dataObj.EvasionHits = EvaHits[1];
            //            dataObj.DPS = CalculateDPS(dataObj.PlayerName);
            //        }
            //    }
            //}

            ParseResultDataGrid.Items.Refresh();
        }

        private void RefreshStatsScreen()
        {
            if (AggregatedDataList.Count > 0)
            {
                List<string> players = AggregatedDataList.Select(o => o.PlayerName).ToList();
                List<ParseModel> tempList = new List<ParseModel>(ParsedObjects).Where(u => players.Any(p => p == u.Attacker)).ToList();

                int topTotalDamage = AggregatedDataList.Max(x => x.TotalDamage);
                int highestDamage = tempList.Max(x => x.Damage);
                string topAcc = AggregatedDataList.Max(x => x.MeleeAccuracy);

                labelTopDD.Content = AggregatedDataList.FirstOrDefault(dmg => dmg.TotalDamage == topTotalDamage).PlayerName + " (" + topTotalDamage + " dmg)";
                labelBestAcc.Content = AggregatedDataList.FirstOrDefault(acc => acc.MeleeAccuracy == topAcc).PlayerName + " (" + topAcc + "% Acc)";
                labelTopDmg.Content = tempList.FirstOrDefault(dmg => dmg.Damage == highestDamage).Attacker + " (" + highestDamage + " dmg)";
            }

            double[] expStats = CalculateExp();

            labelExp.Content = "Exp/h: " + Math.Round(expStats[0], 2).ToString() +
                                 Environment.NewLine + "Total exp: " + expStats[1] +
                                 Environment.NewLine + "Kills: " + expStats[2];
        }
        #endregion

        #region Calculations
        private string[] CalculateMeeleAccuracy(string playerName)
        {
            double accuracy = 0;
            AttackType type = AttackType.Melee;
            List<ParseModel> tempList = new List<ParseModel>(ParsedObjects);

            List<ParseModel> hits = tempList.Where(x => x.Attacker == playerName).Where(x => x.AtkType == type).Where(x => x.Time > fromTimeCached && x.Time < toTimeCached).ToList();
            int hitCount = hits.Where(x => x.AtkRes == AttackResult.Hit).Count();

            if (hitCount > 0)
            {
                accuracy = ((double)hitCount / (double)hits.Count);
            }

            string[] returnString = new string[2];
            returnString[0] = Math.Round((accuracy * 100), 2).ToString() + "%";
            returnString[1] = "(" + hitCount + "/" + hits.Count + ")";

            return returnString;
        }

        private string CalculateRangedAccuracy(string playerName)
        {
            double accuracy = 0;
            AttackType type = AttackType.Range;
            List<ParseModel> tempList = new List<ParseModel>(ParsedObjects);

            List<ParseModel> hits = tempList.Where(x => x.Attacker == playerName).Where(x => x.AtkType == type).Where(x => x.Time > fromTimeCached && x.Time < toTimeCached).ToList();
            int hitCount = hits.Where(x => x.AtkRes == AttackResult.Hit).Count();

            if (hitCount > 0)
            {
                accuracy = ((double)hitCount / (double)hits.Count);
            }

            string returnString = Math.Round((accuracy * 100), 2).ToString() + "%";

            return returnString;
        }

        private string[] CalculateEvasion(string playerName)
        {
            double evasion = 1;
            List<ParseModel> tempList = new List<ParseModel>(ParsedObjects);

            List<ParseModel> hits = tempList.Where(x => x.Target == playerName).Where(x => x.Time > fromTimeCached && x.Time < toTimeCached).ToList();
            int missCount = hits.Where(x => x.AtkRes == AttackResult.Miss).Count();

            if (missCount > 0)
            {
                evasion = ((double)missCount / (double)hits.Count);
            }

            string[] returnString = new string[2];
            returnString[0] = Math.Round((evasion * 100), 2).ToString() + "%";
            returnString[1] = "(" + missCount + "/" + hits.Count + ")";

            return returnString;
        }

        private double[] CalculateExp()
        {
            // stats[0] = Exp/h
            // stats[1] = Total Exp
            // stats[2] = Number of kills
            double[] stats = new double[3];
            double expPerHour = 0;
            double totalExp = 0;
            double kills = 0;

            List<ExpModel> expTempList = ExpList.Where(x => x.Time > fromTimeCached && x.Time < toTimeCached).ToList();

            if (expTempList.Count > 0)
            {
                DateTime firstTime = expTempList.Min(x => x.Time);
                DateTime highestTime = expTempList.Max(x => x.Time);
                TimeSpan span = highestTime - firstTime;

                if (span.TotalMinutes > 0)
                {
                    expPerHour = ((double)expTempList.Sum(x => x.Exp) / (double)span.TotalMinutes) * (double)60;
                    totalExp = (double)expTempList.Sum(x => x.Exp);
                    kills = expTempList.Count;
                }
            }

            stats[0] = expPerHour;
            stats[1] = totalExp;
            stats[2] = kills;

            return stats;
        }

        private double CalculateDPS(string playerName)
        {
            double dps = 0;
            List<ParseModel> tempList = new List<ParseModel>(ParsedObjects);
            List<ParseModel> hits = tempList.Where(x => x.Attacker == playerName).Where(x => x.Time > fromTimeCached && x.Time < toTimeCached).ToList();

            if (hits.Count > 0)
            {
                DateTime firstHit = hits.Min(x => x.Time);
                DateTime lastHit = hits.Max(x => x.Time);
                TimeSpan totalSeconds = lastHit - firstHit;
                int totalDamage = hits.Sum(x => x.Damage);

                dps = (double)totalDamage / (double)totalSeconds.Seconds;
            }

            return Math.Round(dps, 2);
        }
        #endregion
    }
}