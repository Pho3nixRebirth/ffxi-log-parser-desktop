﻿using FFXI_Parser_Desktop.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FFXI_Parser_Desktop
{
    /// <summary>
    /// Interaction logic for History.xaml
    /// </summary>
    public partial class History : Window
    {
        public History(List<ParseModel> PrsdObj, DateTime from, DateTime to, List<ExpModel> expObj = null)
        {
            InitializeComponent();

            if (PrsdObj == null && expObj != null)
            {
                List<ExpModel> ParsedObjects = expObj.Where(x => x.Time > from && x.Time < to).ToList();
                dataGridHistory.ItemsSource = ParsedObjects;
            }
            else
            {
                List<ParseModel> ParsedObjects = PrsdObj.Where(x => x.Time > from && x.Time < to).ToList();
                dataGridHistory.ItemsSource = ParsedObjects;
            }
        }

        private void dataGridHistory_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Time")
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "yyyy-MM-dd HH:mm:ss";
            }
        }
    }
}
